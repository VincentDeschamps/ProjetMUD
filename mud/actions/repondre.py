# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import RepondreEvent

class RepondreAction(Action3):
    EVENT = RepondreEvent
    ACTION = "repondre"
    RESOLVE_OBJECT = "resolve_for_operate"

    def __init__(self, subject, object, object2):
        super().__init__(subject, object, object2 )

    def resolve_object2(self):
        return self.object2
