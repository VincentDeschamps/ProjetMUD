# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class DescendreEvent(Event2):
    NAME = "descendre"

    def perform(self):
        if not self.object.has_prop("montable") or not self.actor.has_prop("sur_objet"):
            self.fail()
            return self.inform("descendre.failed")
        self.actor.remove_prop("sur_objet")
        self.inform("descendre")
