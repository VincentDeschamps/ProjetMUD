# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class RepondreEvent(Event3):
    NAME = "repondre"

    def perform(self):
        if (not self.object2=="oui" and not self.object2=="non") or (not self.object.has_prop("repondable")):
            self.fail()
            return self.inform("repondre.failed")
        self.add_prop("repondu-"+self.object2)
        self.inform("repondre")
