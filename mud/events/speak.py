# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class SpeakEvent(Event2):
    NAME = "speak"

    def perform(self):
        if not self.object.has_prop("speaker"):
            self.fail()
            return self.inform("speak.failed")
        self.inform("speak")
