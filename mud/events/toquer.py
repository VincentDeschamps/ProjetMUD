
from .event import Event2

class ToquerEvent(Event2):
    NAME = "toquer"

    def perform(self):
        if not self.object.has_prop("toquable") and not self.object.has_prop("closed"):
            self.fail()
            return self.inform("toquer.failed")
        self.inform("toquer")
